# Reverse a list in python Input: [4, 5, 6, 7, 8, 9]


list_to_rev = [4, 5, 6, 7, 8, 9]

i = 0

j = len(list_to_rev)-1

while i < j :
    list_to_rev[i],list_to_rev[j] = list_to_rev[j],list_to_rev[i]
    i += 1
    j -= 1

print(list_to_rev)


# Q. what is differnce between = & ==