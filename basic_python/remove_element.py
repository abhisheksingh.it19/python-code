# Remove Multiple Element from a list in python input: [12, 15, 3, 10] Remove [12, 3] & output = [15, 10]

input_list = [12, 15, 3, 10]
remove_list = [12, 3]

output_list = []

for i in range(len(input_list)):
    #remove_flag = False
    if input_list[i] not in remove_list:
        #remove_flag = True
        output_list.append(input_list[i])
print(output_list)